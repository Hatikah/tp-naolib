<?php
include 'api/function.php'
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/style.min.css">
    <title>Liste des lignes</title>
</head>
<body>


    <header role="banner">
        
    </header>

    <main role="main">

        <section class="liste">

            <h2 class="tc">Liste des lignes du réseau</h2>

            <?php
                $lignes = getLignes();
                
                echo '<p class="tc">Nombres de résultats : <span class="liste__total">'. count($lignes) .'</span></p>';
                ?>
            <ul class="lignes">
                <?php

                if(count($lignes) > 0) {

                    foreach($lignes as $key => $ligne) {
                        /** @todo: completer la boucle qui permet de créer un élément li et de l'ajouter à la liste */        
                    }

                }

                ?>
            </ul>

            <div class="tc">
                
                <button class="liste__plus">Plus de résultats</button>
                
            </div>
            
        </section>

    </main>

    <footer role="contentinfo"></footer>

    <script src="api/function.min.js"></script>
    
</body>
</html>
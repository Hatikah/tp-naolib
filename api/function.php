<?php
/**
 * Fonction permettant de récupérer les lignes de bus
 */
function getLignes(){
    /** @todo: completer $url par la bonne url de l'api permettant d'afficher les lignes du réseau */
    $url = '';
    $options = [
        'http'=>[
            'method'=>'GET',
        ]
    ];

    if(@$url){
        $context = stream_context_create($options);
        $contents = file_get_contents($url, false, $context);
        $response = json_decode($contents, true);
        return $response['results'];
    }

    return [];

}

/**
 * Retourne le texte contrasté d'une couleur
 */
function contrastFinder($hex){

    $r = hexdec( substr("$hex", 0, 2) );
    $g = hexdec( substr("$hex", 2, 2) );
    $b = hexdec( substr("$hex", 4, 2) );

    return ((($r + $g + $b)/3) < 128 ? 'ffffff' : '000000' );

}

?>